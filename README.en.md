# vue-element-admin

[简体中文](./README.md) | [English](./README.en.md)

## Project description

Simplify the official project [vue - element - admin] (https://github.com/PanJiaChen/vue-element-admin) background template

## Projects show

[demo](https://xiuguoy.github.io/#/login)

## Project start

```bash
# clone the project
git clone https://gitee.com/yxiuguo/vue-element-admin.git

# enter the project directory
cd vue-element-admin

# install dependency
npm install

# develop
npm run dev
```

This will automatically open http://localhost:9527

## Build

```bash
# build for test environment
npm run build:stage

# build for production environment
npm run build:prod
```



## Project screenshots

![avatar](./public/screen.png)

![avatar](./public/screen1.png)

## License

[MIT](./LICENSE)
