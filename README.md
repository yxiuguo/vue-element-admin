# vue-element-admin

[简体中文](./README.zh-CN.md) | [English](./README.en.md)

## 项目描述

简化官方项目[vue-element-admin](https://github.com/PanJiaChen/vue-element-admin)后台模版

## 项目展示

[演示](https://xiuguoy.github.io/#/login)

## 项目启动

```bash
# 克隆项目
git clone https://gitee.com/yxiuguo/vue-element-admin.git

# 进入项目目录
cd vue-element-admin

# 安装依赖
npm install

# 启动服务
npm run dev

浏览器访问 http://localhost:9527

```

## 发布

```bash
# 构建测试环境
npm run build:stage

# 构建生产环境
npm run build:prod
```

## 项目截图

![avatar](./public/screen.png)

![avatar](./public/screen1.png)


## License

[MIT](./LICENSE)
