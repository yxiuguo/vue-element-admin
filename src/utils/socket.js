import Store from '@/store'

let websock = null;
// socket 连接地址
let wsUrl = 'ws://121.40.165.18:8800';
// 默认重连时间3秒
let limitConnect = 3;                                        
// 记录当前重连次数
let timeConnect = 0;                                   
// 解析返回得消息
let messageCallback = function () {};                           
// 心跳发送/返回的信息 服务器和客户端收到的信息内容如果如下 就识别为心跳信息 不要做业务处理
let checkMsg = "ping";
// 连接标识，避免重复连接
let isConnect = false;
// 存储延迟重连
let rec = null

// 初始化socket
function initWebSocket (url) {
    websock.onopen = function (e) {
		// console.log("socket准备好了", e);
    }
    websock.onmessage = function (e) {
        messageCallback(e.data)
		heartCheck.reset().start();
    }
    websock.onclose = function (e) {
        isConnect = false ;
    }
    websock.onerror = function (e) {
        isConnect = false;
		reconnect(url);
    }
}

// 监听浏览器窗口关闭事件。当窗口关闭时，主动关闭websocket连接，防止在连接断开之前关闭窗口，服务器会抛出异常。
window.onbeforeunload = function() {
    let socket = Store.getters.isSocket;
    if(socket){
        closeSocket()
    }
} 

// 断开重连
function reconnect(url) {
    // 如果已经连上就不在重连了
    if (isConnect) return;
    // 防止onclose和oneror重新连接两次
	if(limitConnect > 0 && !isConnect){
		limitConnect --;
		timeConnect ++;
        rec && clearTimeout(rec);
		// 延迟5秒重连，避免过多次过频繁请求重连
		rec = setTimeout(function(){
			connectionSocket(url);
		}, 5000);
	}
}

// 心跳
let heartCheck = {
    timeout: 15000,         // 默认55秒发送一次
    timeoutObj: null,
    reset: function(){
        clearTimeout(this.timeoutObj);
        // this.start();
        return this;
    },
    start: function(){
        this.timeoutObj = setTimeout(function () {
            if (isConnect) websock.send(checkMsg);
        }, this.timeout);
    }
}

// 创建socket
function connectionSocket(url) {
	try {
		websock = new WebSocket(url);
		websock.binaryType = "arraybuffer";
		initWebSocket(url);
        isConnect = true;
        Store.dispatch('socket/setSocket', true)
	} catch (e) {
		reconnect(url);
	}
}

// 关闭socket
function closeSocket(){
    isConnect = false;
    timeConnect = 0;
    websock.close();
    Store.dispatch('socket/setSocket', false)
}

// 开启socket
function openSocket(callback) {
    messageCallback = callback;
	connectionSocket(wsUrl);
}

// 发送socket
function sendSocket (agentData, callback) {
    messageCallback = callback;
    if (websock.readyState === websock.OPEN) {
        // 如果已开启
        websock.send(agentData)
    } else if (websock.readyState === websock.CONNECTING) {
        // 若是正在开启状态，则等待1s后重新调用
        setTimeout(function () {
            sendSocket(agentData, callback)
        }, 1000)
    } else {
        // 若未开启 ，则等待1s后重新调用
        setTimeout(function () {
            sendSocket(agentData, callback)
        }, 1000)
    }
}

// Expose the method
export {
    sendSocket,
	closeSocket,
	openSocket
}