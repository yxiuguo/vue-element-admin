/** When your routing table is too long, you can split it into small modules**/

import Layout from '@/layout'

const otherRouter = {
  path: '/other',
  component: Layout,
  redirect: 'noRedirect',
  name: 'Other',
  meta: {
    title: '其他',
    icon: 'international'
  },
  children: [
    {
      path: 'videoPlayer',
      component: () => import('@/views/other/videoPlayer'),
      name: 'VideoPlayer',
      meta: { title: '视频', noCache: true }
    },
    {
      path: 'table',
      component: () => import('@/views/other/table'),
      name: 'Table',
      meta: { title: '表格', noCache: true }
    },
    {
      path: 'menu',
      component: () => import('@/views/other/menu'),
      name: 'Menu',
      meta: { title: '菜单分离', noCache: true }
    },
    {
      path: 'css',
      component: () => import('@/views/other/cssStyle'),
      name: 'Css',
      meta: { title: 'CSS样式', noCache: true }
    },
    {
      path: 'socket',
      component: () => import('@/views/other/socket'),
      name: 'Socket',
      meta: { title: 'socket', noCache: true }
    },
    {
      path: 'interval',
      component: () => import('@/views/other/interval'),
      name: 'Interval',
      meta: { title: '循环定时器', noCache: true }
    }
  ]
}

export default otherRouter
