/** When your routing table is too long, you can split it into small modules**/

import Layout from '@/layout'

const chartsRouter = {
  path: '/map',
  component: Layout,
  redirect: 'noRedirect',
  name: 'Map',
  meta: {
    title: 'map',
    icon: 'international'
  },
  children: [
    {
      path: 'arcGis',
      component: () => import('@/views/map/arcGis'),
      name: 'ArcGis',
      meta: { title: 'GIS地图', noCache: true }
    },
    {
      path: 'baidu',
      component: () => import('@/views/map/baidu'),
      name: 'Baidu',
      meta: { title: '百度地图', noCache: true }
    },
    {
      path: 'tdt',
      component: () => import('@/views/map/tdt'),
      name: 'Tdt',
      meta: { title: '天地图', noCache: true }
    }
  ]
}

export default chartsRouter
