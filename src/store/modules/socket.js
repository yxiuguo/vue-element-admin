import Cookies from 'js-cookie'

const state = {
  isSocket: false
}

const mutations = {
  SET_SOCKET: (state, flag) => {
    state.isSocket = flag;
  }
}

const actions = {
  setSocket({ commit }, flag) {
    return new Promise(resolve => {
        commit('SET_SOCKET', flag)
        resolve()
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
