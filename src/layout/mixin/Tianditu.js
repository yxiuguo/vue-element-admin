import store from '@/store'

export default {
    data() {
        return {
            // 图片标注样式
            symbol: {
                type: "picture-marker",
                url: require('@/assets/location.png'),
                width: "22px",
                height: "33px"
            },
            // 圆点标注有边线样式
            symbol3: {
                type: "simple-marker",
                color: [255, 0, 0],
                size: 8,
                outline: {
                    color: [255, 255, 255],
                    width: 1
                }
            },
            // 文字有背景样式
            symbol4: {
                color: [255, 255, 255],
                haloColor: 'black',
                haloSize: '30px',
                font: {
                    size: '14px',
                    weight: 'bolder'
                },
                xoffset: '35px',
                yoffset: '-5px',
                labelPlacement: "center-center",
                // labelExpressionInfo: {
                //     expression: "$feature.cluster_type_fuel1"
                // },
                // where: `cluster_avg_capacity_mw > 1500`
            },
            // 透明背景圆点
            symbol5: {
                // 圆心填充色
                color: [105, 220, 255],
                size: 24,
                angle: 0,
                xoffset: 0,
                yoffset: 0,
                style: 'circle',
                // 圆边缘色
                outline: {
                    color: [0, 139, 174, 0.5],
                    width: 5
                }
            },
            // 文字无背景样式
            symbol6: {
                color: [0, 74, 93, 1],
                font: {
                    size: '14px',
                    weight: 'bolder'
                },
                xoffset: 0,
                yoffset: '-5px',
                labelPlacement: "center-center",
            },
            // 线样式
            polylineConfig: {
                // 线条配置
                symbol: {
                    type: "simple-line",
                    color: [0, 139, 174, 1],
                    width: 3
                },
                // 弹窗信息配置
                popupTemplate: {
                    title: "{Name}",
                    content: [{
                        type: "fields",
                        fieldInfos: [{
                            fieldName: "Name"
                        }, {
                            fieldName: "Owner"
                        }, {
                            fieldName: "Length"
                        }]
                    }]
                },
                // 弹窗信息数据
                attributes: {},
                // 线起止点数据
                geometry: {
                    type: "polyline",
                    paths: []
                },
            }
        }
    },
    watch: {},
    beforeMount() {},
    beforeDestroy() {},
    mounted() { 
        // 天地图地图服务二级域名包括t0-t7，可以随机选择使用
        const subDomains = ["t0", "t1", "t2", "t3", "t4", "t5", "t6", "t7"];
        // 天地图服务地址
        const tiandituBaseUrl = `http://${subDomains}.tianditu.gov.cn`;
        // 天地图key
        const token = "932f29e514bd23df81ea0df4eb29f17e";
        // 地图的投影方式 EPSG:900913(墨卡托投影)，EPSG:4326(大地平面投影)
        const wkid = 4326;
        // 墨卡托投影矢量-底图
        this.tiledLayer = {
            urlTemplate: tiandituBaseUrl + '/DataServer?T=img_w/wmts&x={col}&y={row}&l={level}&tk=' + token,
            subDomains: subDomains,
        };
        // 墨卡托投影-矢量标注
        this.tiledLayerAnno = {
            urlTemplate: tiandituBaseUrl + "/DataServer?T=cia_w?T=vec_c/wmts&x={col}&y={row}&l={level}&tk=" + token,
            subDomains: subDomains,
        };
        // 瓦片矢量-底图
        this.veccLayer = {
            urlTemplate: tiandituBaseUrl + "/vec_c/wmts?SERVICE=WMTS&REQUEST=GetTile&VERSION=1.0.0&LAYER=vec&STYLE=default&TILEMATRIXSET=c&FORMAT=tiles&TILECOL={col}&TILEROW={row}&TILEMATRIX={level}&tk=" + token,
            subDomains: subDomains, 
        };
        // 瓦片地图-矢量标注
        this.cvacLayer = {
            urlTemplate:  tiandituBaseUrl + "/cva_c/wmts?SERVICE=WMTS&REQUEST=GetTile&VERSION=1.0.0&LAYER=cva&STYLE=default&TILEMATRIXSET=c&FORMAT=tiles&TILECOL={col}&TILEROW={row}&TILEMATRIX={level}&tk=" + token,
            subDomains: subDomains,
        };
        // 
        this.fullExtent = (-180.0, -90.0, 180.0, 90.0, { wkid: wkid });
        // 瓦片信息
        this.tileInfos = {
            rows: 256,
            cols: 256,
            compressionQuality: 0,
            origin: {
                x: -180,
                y: 90
            },
            spatialReference: {
                wkid: 4326
            },
            lods: [
                { level: 1, scale: 295829355.45, resolution: 0.703125 },
                { level: 2, scale: 147914677.725, resolution: 0.3515625 },
                { level: 3, scale: 73957338.8625, resolution: 0.17578125 },
                { level: 4, scale: 36978669.43125, resolution: 0.087890625 },
                { level: 5, scale: 18489334.715625, resolution: 0.0439453125 },
                { level: 6, scale: 9244667.3578125, resolution: 0.02197265625 },
                { level: 7, scale: 4622333.67890625, resolution: 0.010986328125 },
                { level: 8, scale: 2311166.839453125, resolution: 0.0054931640625 },
                { level: 9, scale: 1155583.4197265625, resolution: 0.00274658203125 },
                { level: 10, scale: 577791.7098632812, resolution: 0.001373291015625 },
                { level: 11, scale: 288895.8549316406, resolution: 0.0006866455078125 },
                { level: 12, scale: 144447.9274658203, resolution: 0.00034332275390625 },
                { level: 13, scale: 72223.96373291015, resolution: 0.000171661376953125 },
                { level: 14, scale: 36111.98186645508, resolution: 0.0000858306884765625 },
                { level: 15, scale: 18055.99093322754, resolution: 0.00004291534423828125 },
                { level: 16, scale: 9027.99546661377, resolution: 0.000021457672119140625 },
                { level: 17, scale: 4513.997733306885, resolution: 0.000010728836059570312 },
                { level: 18, scale: 2256.9988666534423, resolution: 0.000005364418029785156 },
                { level: 19, scale: 1128.4994333267211, resolution: 0.000002682209014892578 }
            ]
        };

    },
    methods: {
        
    }
}
